#!/bin/bash

for f in $(find ../kicad-color-schemes -type f -iname '*.json'); do
    filename=$(basename "$f")
    name="${filename%.*}"
    mkdir -p theme-$name/1.0/colors
    cp "$f" "theme-$name/1.0/colors/$filename"
    cat <<EOF > "theme-$name/1.0/metadata.json"
{
    "\$schema": "https://gitlab.com/qu1ck/kicad/-/raw/pcm/kicad/pcm/schemas/pcm.v1.schema.json#",
    "name": "$name theme",
    "description": "$name color theme",
    "description_full": "$name color theme",
    "identifier": "theme-$name",
    "type": "colortheme",
    "author": {
        "name": "Thomas Pointhuber",
        "contact": {
            "email": "john@doe.com",
            "web": "johndoe.com"
        }
    },
    "maintainer": {
        "name": "qu1ck",
        "contact": {
            "email": "mail@qu1ck.org"
        }
    },
    "license": "CC0-1.0",
    "resources": {
        "Github": "https://github.com/pointhi/kicad-color-schemes"
    },
    "versions": [{
        "version": "1.0",
        "status": "stable",
        "kicad_version": "5.99"
    }]
}
EOF
done
