import os
import io
import json
import hashlib
from zipfile import ZipFile

os.chdir(os.path.dirname(__file__))

READ_SIZE = 65536


def getsha256(filename):
    hash = hashlib.sha256()
    with io.open(filename, "rb") as f:
        data = f.read(READ_SIZE)
        while data:
            hash.update(data)
            data = f.read(READ_SIZE)
    return hash.hexdigest()


def get_zip_contents_size(path):
    result = 0
    try:
        with ZipFile(path, 'r') as zip:
            for info in zip.infolist():
                result += info.file_size
    except:
        pass
    return result


_, packages, _ = next(os.walk("."))

packages = [p for p in packages if p not in ["zip", ".git"]]

all_packages = []

for package in packages:
    _, versions, _ = next(os.walk(package))
    package_json = None
    for version in versions:
        print("{}: {}".format(package, version))
        with io.open("{}/{}/metadata.json".format(package, version), "r", encoding="utf-8") as f:
            js = json.load(f)

        if js["versions"]:
            zipname = f"zip/{package}_v{version}.zip"
            js["versions"][0]["download_url"] = f"https://qu1ck.org/share/pcm/{zipname}"
            js["versions"][0]["download_sha256"] = getsha256(zipname)
            js["versions"][0]["download_size"] = os.path.getsize(zipname)
            js["versions"][0]["install_size"] = get_zip_contents_size(zipname)

            if js["identifier"] == "dummy-plugin-4" and js["versions"][0]["version"] == "1.0":
                js["versions"][0]["download_sha256"] = "deadbeef" * 8

            if package_json is None:
                package_json = js
            else:
                package_json["versions"].append(js["versions"][0])
        else:
            package_json = js

    all_packages.append(package_json)

with io.open("../repository/packages.json", "w", encoding="utf-8") as f:
    json.dump({"packages": all_packages}, f, indent=4)


def execfile(filepath, globals=None, locals=None):
    if globals is None:
        globals = {}
    globals.update({
        "__file__": filepath,
        "__name__": "__main__",
    })
    with open(filepath, 'rb') as file:
        exec(compile(file.read(), filepath, 'exec'), globals, locals)


execfile("../repository/updatehash.py")
