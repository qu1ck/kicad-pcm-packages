#!/bin/bash

cd $(dirname "$0")

for pkg in $(find . -mindepth 2 -maxdepth 2 -type d -printf '%P\n' | grep -v .git); do
    cd "$pkg"
    zipname=$(echo "$pkg" | sed 's/\//_v/g')
    if [ ! -f "../../zip/$zipname.zip" ]; then
        zip -9 -R "../../zip/$zipname.zip" \* -x \*.pyc
    fi
    cd ../..
done

if [ ! -f "zip/resources.zip" ]; then
    find -mindepth 2 -maxdepth 2 -type f -name icon.png | xargs zip -9 "zip/resources.zip"
fi

dd if=/dev/zero of="zip/dummy-plugin-4_v2.0.zip" bs=1024 count=1

python3 ./updatepackages.py

scp -r zip/ srv:/mnt/int/www/pcm/
