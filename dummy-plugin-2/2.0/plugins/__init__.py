import os
import pcbnew
import wx

class Plugin2(pcbnew.ActionPlugin):

    def defaults(self):
        self.name = "Dummy Plugin 2"
        self.category = "Read PCB"
        self.description = ""
        self.icon_file_name = os.path.join(os.path.dirname(__file__), 'icon.png')

    def Run(self):
        wx.MessageBox("Plugin 2")

Plugin2().register()
