import os
import pcbnew
import wx

class Plugin2(pcbnew.ActionPlugin):

    def defaults(self):
        self.name = "Update test plugin"
        self.category = "Read PCB"
        self.description = ""
        self.icon_file_name = os.path.join(os.path.dirname(__file__), 'icon.png')
        self.show_toolbar_button = True

    def Run(self):
        wx.MessageBox("Update test plugin. Will create a dummy.ini.")
        d = os.path.dirname(__file__)
        with open(os.path.join(d, "dummy.ini"), "w") as f:
            f.write("created by update test plugin v2")


Plugin2().register()

if __name__ == '__main__':
  Plugin2().Run()
