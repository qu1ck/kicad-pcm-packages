import os
import pcbnew
import wx

class Plugin2(pcbnew.ActionPlugin):

    def defaults(self):
        self.name = "Dummy Plugin 3"
        self.category = "Read PCB"
        self.description = ""
        self.icon_file_name = os.path.join(os.path.dirname(__file__), 'icon.png')
        self.show_toolbar_button = True

    def Run(self):
        wx.MessageBox("Plugin 3")

Plugin2().register()
