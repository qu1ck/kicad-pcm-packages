import os
import pcbnew
import wx

class Plugin2(pcbnew.ActionPlugin):

    def defaults(self):
        self.name = "Dummy Plugin 1"
        self.category = "Read PCB"
        self.description = ""
        self.icon_file_name = os.path.join(os.path.dirname(__file__), 'icon.png')
        self.dark_icon_file_name = os.path.join(os.path.dirname(__file__), 'icon_dark.png')
        self.show_toolbar_button = True

    def Run(self):
        wx.MessageBox("Plugin 1")

#Plugin2().register()
if __name__ == '__main__':
  Plugin2().Run()
